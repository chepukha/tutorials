package edu.gwu.ndk.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.gwu.ndk.model.Login;

@Controller
@SessionAttributes("login")
public class LoginController {    
    
	@RequestMapping(value = "login")
    public String login(Model model) {
		Login login = new Login();
		model.addAttribute("login", login);
        System.out.println("Calling login page");
		return "login";
    }
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String submit(@ModelAttribute("login") Login login) {
		System.out.println("Username: "+login.getUsername());
		System.out.println("Pwd: "+login.getPwd());
		
		return "redirect:confirm.html";
	}
}
